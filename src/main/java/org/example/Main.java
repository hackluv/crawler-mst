package org.example;

import org.json.JSONObject;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class Main {
    public static final String BASE_URL = "https://masothue.com/";
    public static final String TOKEN_PATH_COMPARE = "Ajax/Token";
    public static final String PATH_SEARCH = "Search";
    public static final String TAX_NUMBER = "0110502919";
    public static final String TOKEN_INPUT_ELEMENT = "input[name=token]";
    public static final String USER_AGENT = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/95.0.4638.54 Safari/537.36";

    public enum EMethod {
        POST("post"),
        GET("get"),
        PUT("put"),
        DELETE("delete");
        private final String text;

        EMethod(final String text) {
            this.text = text;
        }

        @Override
        public String toString() {
            return text;
        }
    }

    public enum EType {
        AUTO("auto"), // tự động
        ENTERPRISE_TAX_NUMBER("enterpriseTax"), // mã số thuế doanh nghiệp
        PERSONAL_TAX_NUMBER("personalTax"), // mã số thuế cá nhân
        IDENTITY_CARD_NUMBER("identity"), // căn cước công dân
        ENTERPRISE_NAME("enterpriseName"), // tên doanh nghiệp
        LEGAL_NAME("legalName"); // tên giám đốc công ty

        private final String text;

        EType(final String text) {
            this.text = text;
        }

        @Override
        public String toString() {
            return text;
        }
    }

    public static void main(String[] args) {
        Map<String, Object> data = new HashMap<>();
        ArrayList<Map<String, Object>> relatedOccupations = new ArrayList<>();
        String token = "";
        String forceSearch = "1"; // only 0 or 1
        String rand = randomString(6);
        Connection.Response response = null;
        try {
            response = Jsoup.connect(BASE_URL + TOKEN_PATH_COMPARE)
                    .data("r", rand)
                    .method(Connection.Method.POST)
                    .execute();
            String responseCode = response.body();
            JSONObject obj = new JSONObject(responseCode);
            token = obj.getString("token");
            String responseSearch = Jsoup.connect(BASE_URL + PATH_SEARCH + "/")
                    .data("token", token)
                    .data("force-search", forceSearch)
                    .data("type", EType.ENTERPRISE_TAX_NUMBER.toString())
                    .data("q", TAX_NUMBER).get().body().html();
            Document document = Jsoup.parse(responseSearch);
            Element tableTaxInfo = document.select("#main .table-taxinfo").first();
            if (tableTaxInfo != null) {
                Element name = tableTaxInfo.select("thead tr th[itemprop='name']").first();
                Element globalName = tableTaxInfo.select("tbody td[itemprop=alternateName]").first();
                Element shortName = tableTaxInfo.select("tbody td[itemprop=alternateName]").get(1);
                Element taxId = tableTaxInfo.select("tbody td[itemprop=taxId]").first();
                Element address = tableTaxInfo.select("tbody td[itemprop=address]").first();
                Element telephone = tableTaxInfo.select("tbody td[itemprop=telephone]").first();
                Element deputy = tableTaxInfo.select("tbody tr[itemprop=alumni] td span[itemprop=name]").first();
                Element trOperatingDay = document.select("tr:contains(Ngày hoạt động)").first().select("td:eq(1)").first();
                Element trManagedBy = document.select("tr:contains(Quản lý bởi)").first().select("td:eq(1)").first();
                Element trEnterpriseType = document.select("tr:contains(Loại hình DN)").first().select("td:eq(1)").first();
                Element trStatus = document.select("tr:contains(Tình trạng)").first().select("td:eq(1)").first();
                Elements tdUpdateAtElements = document.select("td[colspan]:contains(Cập nhật mã số thuế)");
                if (name.text() != null) data.put("name", name.text());
                if (globalName.text() != null) data.put("globalName", globalName.text());
                if (shortName.text() != null) data.put("shortName", shortName.text());
                if (taxId.text() != null) data.put("taxId", taxId.text());
                if (address.text() != null) data.put("address", address.text());
                if (deputy.text() != null) data.put("deputy", deputy.text());
                if (telephone.text() != null) data.put("telephone", telephone.text());
                if (trOperatingDay.text() != null) data.put("operatingDay", trOperatingDay.text());
                if (trManagedBy.text() != null) data.put("managedBy", trManagedBy.text());
                if (trEnterpriseType.text() != null) data.put("enterpriseType", trEnterpriseType.text());
                if (trStatus.text() != null) data.put("status", trStatus.text());
                if (!tdUpdateAtElements.isEmpty()) {
                    Element emUpdateElementFirst = tdUpdateAtElements.first().select("em").first();
                    data.put("updateAt", emUpdateElementFirst.text());
                }
            } else {
                data.put("statusCode", 404);
                data.put("message", "Enterprise not found");
                System.out.println(new JSONObject(data));
                return;
            }
            Element tableRelatedOccupations = document.select("#main .table").first();
            if (tableRelatedOccupations != null) {
                Elements trElements = tableRelatedOccupations.select("tbody tr");
                for (Element tr : trElements) {
                    Elements tdElements = tr.select("td");
                    if (tdElements.size() == 2) {
                        Map<String, Object> relatedOccupation = new HashMap<>();
                        Element firstTd = tdElements.first();
                        Element secondTd = tdElements.get(1);
                        String valueFromFirstTd = firstTd.text();
                        String valueFromSecondTd = secondTd.text();
                        relatedOccupation.put("code", valueFromFirstTd);
                        relatedOccupation.put("industry", valueFromSecondTd);
                        relatedOccupations.add(relatedOccupation);
                    }
                }
            }
            data.put("industries", relatedOccupations);
            JSONObject responseJson = new JSONObject(data);
            System.out.println(responseJson);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static String randomString(int length) {
        String AlphaNumericString = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
                + "0123456789"
                + "abcdefghijklmnopqrstuvxyz";
        StringBuilder sb = new StringBuilder(length);
        for (int i = 0; i < length; i++) {
            int index
                    = (int) (AlphaNumericString.length()
                    * Math.random());
            sb.append(AlphaNumericString
                    .charAt(index));
        }
        return sb.toString();
    }
}